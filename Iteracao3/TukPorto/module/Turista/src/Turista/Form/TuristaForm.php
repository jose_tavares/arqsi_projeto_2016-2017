<?php
namespace Turista\Form;

use Zend\Form\Form;

class TuristaForm extends Form
{

    public function __construct($name = null)
    {
        parent::__construct('turista');
        
        $this->add(array(
            'name' => 'idTurista',
            'type' => 'Hidden'
        ));
        
        $this->add(array(
            'name' => 'nome',
            'type' => 'Text',
            'options' => array(
                'label' => 'Nome'
            )
        ));
        
        $this->add(array(
            'name' => 'nacionalidade',
            'type' => 'Text',
            'options' => array(
                'label' => 'Nacionalidade'
            )
        ));
        
        $this->add(array(
            'name' => 'email',
            'type' => 'Text',
            'options' => array(
                'label' => 'Email'
            )
        ));
        
        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Go',
                'id' => 'submitbutton',
            ),
        ));
        
    }
}