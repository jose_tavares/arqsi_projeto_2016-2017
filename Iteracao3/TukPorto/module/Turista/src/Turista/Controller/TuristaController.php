<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/Turista for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
namespace Turista\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Db\Adapter\Adapter as DbAdapter;
use Zend\Authentication\Adapter\DbTable\CredentialTreatmentAdapter as AuthAdapter;
use Turista\Model\Turista;
use Turista\Form\TuristaForm;
use Turista\Form\LoginForm;

class TuristaController extends AbstractActionController
{

    protected $turistaTable;

    public function getTuristaTable()
    {
        if (! $this->turistaTable) {
            $sm = $this->getServiceLocator();
            $this->turistaTable = $sm->get('Turista\Model\TuristaTable');
        }
        return $this->turistaTable;
    }

    public function indexAction()
    {
        return new ViewModel(array(
            'turistas' => $this->getTuristaTable()->fetchAll()
        ));
    }

    public function addAction()
    {
        $form = new TuristaForm();
        $form->get('submit')->setValue('Novo');
        
        $request = $this->getRequest();
        if ($request->isPost()) {
            $turista = new Turista();
            $form->setInputFilter($turista->getInputFilter());
            $form->setData($request->getPost());
            
            if ($form->isValid()) {
                $turista->exchangeArray($form->getData());
                $this->getTuristaTable()->saveTurista($turista);
                
                return $this->redirect()->toRoute('turista');
            }
        }
        return array(
            'form' => $form
        );
    }

    public function editAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);
        if (! $id) {
            return $this->redirect()->toRoute('turista', array(
                'action' => 'index'
            ));
        }
        try {
            $turista = $this->getTuristaTable()->getTurista($id);
        } catch (\Exception $ex) {
            return $this->redirect()->toRoute('turista', array(
                'action' => 'index'
            ));
        }
        $form = new TuristaForm();
        $form->bind($turista);
        $form->get('submit')->setAttribute('value', 'Editar');
        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setInputFilter($turista->getInputFilter());
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $this->getTuristaTable()->saveTurista($turista);
                return $this->redirect()->toRoute('turista');
            }
        }
        return array(
            'id' => $id,
            'form' => $form
        );
    }

    public function deleteAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);
        if (! $id) {
            return $this->redirect()->toRoute('turista');
        }
        $request = $this->getRequest();
        if ($request->isPost()) {
            $del = $request->getPost('del', 'N�o');
            if ($del == 'Sim') {
                $id = (int) $request->getPost('id');
                $this->getTuristaTable()->deleteTurista($id);
            }
            return $this->redirect()->toRoute('turista');
        }
        return array(
            'id' => $id,
            'turista' => $this->getTuristaTable()->getTurista($id)
        );
    }

    public function fooAction()
    {
        // This shows the :controller and :action parameters in default route
        // are working when you browse to /turista/turista/foo
        return array();
    }

    public function loginAction()
    {
        $request = $this->getRequest();
        if (! $request->isPost()) {
            $form = new LoginForm();
            $form->get('submit')->setValue('Entrar');
            return array(
                'form' => $form
            );
        } else {
            $name = $request->getPost('username');
            $pass = $request->getPost('password');
            $dbAdapter = new DbAdapter([
                'driver' => 'Pdo',
                'dsn' => 'mysql:dbname=zf2tukporto;host=localhost;port=3306',
                'username' => 'zf2',
                'password' => 'zf2'
            ]);
            $authAdapter = new AuthAdapter($dbAdapter);
            $authAdapter->setTableName('users')
                ->setIdentityColumn('username')
                ->setCredentialColumn('password');
            $authAdapter->setIdentity($name)->setCredential($pass);
            $result = $authAdapter->authenticate();
            if ($result->isValid()) {
                return $this->redirect()->toRoute('turista');
            } else {
                return $this->redirect()->toRoute('turista', array(
                    'controller' => 'Turista',
                    'action' => 'login'
                ));
            }
        }
    }
}
