<?php
namespace Turista\Model;

use Zend\Db\TableGateway\TableGateway;

class TuristaTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function getTurista($idTurista)
    {
        $id = (int) $idTurista;
        $rowSet = $this->tableGateway->select(array(
            'idTurista' => $id
        ));
        $row = $rowSet->current();
        if (! $row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function saveTurista(Turista $turista)
    {
        $data = array(
            'nome' => $turista->nome,
            'nacionalidade' => $turista->nacionalidade,
            'email' => $turista->email
        );
        
        $id = (int) $turista->idTurista;
        if ($id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->getTurista($id)) {
                $this->tableGateway->update($data, array(
                    'idTurista' => $id
                ));
            } else {
                throw new \Exception("Turista id does not exist");
            }
        }
    }

    public function deleteTurista($id)
    {
        $this->tableGateway->delete(array(
            'idTurista' => (int) $id
        ));
    }
}

