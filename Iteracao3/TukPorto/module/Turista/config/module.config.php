<?php
return array(
    'controllers' => array(
        'invokables' => array(
            'Turista\Controller\Turista' => 'Turista\Controller\TuristaController'
        )
    ),
    'router' => array(
        'routes' => array(
            'turista' => array(
                'type' => 'segment',
                'options' => array(
                    // Change this to something specific to your module
                    'route' => '/turista[/:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+'
                    ),
                    'defaults' => array(
                        'controller' => 'Turista\Controller\Turista',
                        'action' => 'index'
                    )
                )
            )
        )
    ),
    
    'view_manager' => array(
        'template_path_stack' => array(
            'Turista' => __DIR__ . '/../view'
        )
    )
);
