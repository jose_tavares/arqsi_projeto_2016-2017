<?php
return array(
    'controllers' => array(
        'invokables' => array(
            'Passagem\Controller\Passagem' => 'Passagem\Controller\PassagemController'
        )
    ),
    'router' => array(
        'routes' => array(
            'passagem' => array(
                'type' => 'segment',
                'options' => array(
                    // Change this to something specific to your module
                    'route' => '/passagem[/:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+'
                    ),
                    'defaults' => array(
                        'controller' => 'Passagem\Controller\Passagem',
                        'action' => 'index'
                    )
                )
            )
        )
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'Passagem' => __DIR__ . '/../view'
        )
    )
);
