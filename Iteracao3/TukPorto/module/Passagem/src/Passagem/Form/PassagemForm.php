<?php
namespace Passagem\Form;

use Zend\Form\Form;


class PassagemForm extends Form
{
    public function __construct($name = null)
    {
        parent::__construct('passagem');
        
        $this->add(array(
            'name' => 'idPassagem',
            'type' => 'Hidden'
        ));
        
        $this->add(array(
            'name' => 'nome',
            'type' => 'Text',
            'options' => array(
                'label' => 'Nome'
            )
        ));
        
        $this->add(array(
            'name' => 'descricao',
            'type' => 'Text',
            'options' => array(
                'label' => 'Descricao'
            )
        ));
        
        $this->add(array(
            'name' => 'gps_lat',
            'type' => 'Text',
            'options' => array(
                'label' => 'GPS Latitude'
            )
        ));
        
        $this->add(array(
            'name' => 'gps_long',
            'type' => 'Text',
            'options' => array(
                'label' => 'GPS Longitude'
            )
        ));
        
        $this->add(array(
            'name' => 'idPercurso',
            'type' => 'Text',
            'options' => array(
                'label' => 'Percurso'
            )
        ));
        
        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Go',
                'id' => 'submitbutton'
            )
        ));
    }
}

