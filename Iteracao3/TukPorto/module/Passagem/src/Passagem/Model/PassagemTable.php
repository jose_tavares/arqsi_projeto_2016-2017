<?php
namespace Passagem\Model;

use Zend\Db\TableGateway\TableGateway;
use Passagem\Model;

class PassagemTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function getPassagem($idPassagem)
    {
        $id = (int) $idPassagem;
        $rowSet = $this->tableGateway->select(array(
            'idPassagem' => $id
        ));
        $row = $rowSet->current();
        if (! $row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function savePassagem(Passagem $passagem)
    {
        $data = array(
            'nome' => $passagem->nome,
            'descricao' => $passagem->descricao,
            'gps_lat' => $passagem->gps_lat,
            'gps_long' => $passagem->gps_long,
            'idPercurso' => $passagem->idPercurso
        );
        $id = (int) $passagem->idPassagem;
        if ($id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->getPassagem($id)) {
                $this->tableGateway->update($data, array(
                    'idPassagem' => $id
                ));
            } else {
                throw new \Exception("Passagem id does not exist");
            }
        }
    }
    
    public function deletePassagem($id)
    {
        $this->tableGateway->delete(array(
            'idPassagem' => (int) $id
        ));
    }
}