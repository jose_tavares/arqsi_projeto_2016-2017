<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/Passagem for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
namespace Passagem\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Passagem\Form\PassagemForm;
use Passagem\Model\Passagem;

class PassagemController extends AbstractActionController
{

    protected $passagemTable;

    public function getPassagemTable()
    {
        if (! $this->passagemTable) {
            $sm = $this->getServiceLocator();
            $this->passagemTable = $sm->get('Passagem\Model\PassagemTable');
        }
        return $this->passagemTable;
    }

    public function indexAction()
    {
        return new ViewModel(array(
            'passagens' => $this->getPassagemTable()->fetchAll()
        ));
    }

    public function addAction()
    {
        $form = new PassagemForm();
        $form->get('submit')->setValue('Novo');
        $request = $this->getRequest();
        if ($request->isPost()) {
            $passagem = new Passagem();
            $form->setInputFilter($passagem->getInputFilter());
            $form->setData($request->getPost());
            
            if ($form->isValid()) {
                $passagem->exchangeArray($form->getData());
                $this->getPassagemTable()->savePassagem($passagem);
                
                return $this->redirect()->toRoute('passagem');
            }
        }
        return array(
            'form' => $form
        );
    }

    public function editAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);
        if (! $id) {
            return $this->redirect()->toRoute('passagem', array(
                'action' => 'index'
            ));
        }
        try {
            $passagem = $this->getPassagemTable()->getPassagem($id);
        } catch (\Exception $ex) {
            return $this->redirect()->toRoute('passagem', array(
                'action' => 'index'
            ));
        }
        $form = new PassagemForm();
        $form->bind($passagem);
        $form->get('submit')->setAttribute('value', 'Editar');
        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setInputFilter($passagem->getInputFilter());
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $this->getPassagemTable()->savePassagem($passagem);
                return $this->redirect()->toRoute('passagem');
            }
        }
        return array(
            'id' => $id,
            'form' => $form
        );
    }
    
    public function deleteAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);
        if (! $id) {
            return $this->redirect()->toRoute('passagem');
        }
        $request = $this->getRequest();
        if ($request->isPost()) {
            $del = $request->getPost('del', 'N�o');
            if ($del == 'Sim') {
                $id = (int) $request->getPost('id');
                $this->getPassagemTable()->deletePassagem($id);
            }
            return $this->redirect()->toRoute('passagem');
        }
        return array(
            'id' => $id,
            'passagem' => $this->getPassagemTable()->getPassagem($id)
        );
    }

    public function fooAction()
    {
        // This shows the :controller and :action parameters in default route
        // are working when you browse to /passagem/passagem/foo
        return array();
    }
}
