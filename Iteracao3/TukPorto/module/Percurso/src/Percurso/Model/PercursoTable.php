<?php
namespace Percurso\Model;

use Zend\Db\TableGateway\TableGateway;
use Percurso\Model;

class PercursoTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function getPercurso($idPercurso)
    {
        $id = (int) $idPercurso;
        $rowSet = $this->tableGateway->select(array(
            'idPercurso' => $id
        ));
        $row = $rowSet->current();
        if (! $row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function savePercurso(Percurso $percurso)
    {
        $data = array(
            'descricao' => $percurso->descricao,
            'data' => $percurso->data,
            'idTurista' => $percurso->idTurista
        );
        
        $id = (int) $percurso->idPercurso;
        if ($id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->getPercurso($id)) {
                $this->tableGateway->update($data, array(
                    'idPercurso' => $id
                ));
            } else {
                throw new \Exception("Percurso id does not exist");
            }
        }
    }
    
    public function deletePercurso($id)
    {
        $this->tableGateway->delete(array(
            'idPercurso' => (int) $id
        ));
    }
}