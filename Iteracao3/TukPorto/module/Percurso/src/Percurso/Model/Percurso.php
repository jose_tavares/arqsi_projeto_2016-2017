<?php
namespace Percurso\Model;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class Percurso implements InputFilterAwareInterface
{

    public $idPercurso;

    public $descricao;

    public $data;

    public $idTurista;

    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->idPercurso = (! empty($data['idPercurso'])) ? $data['idPercurso'] : null;
        $this->descricao = (! empty($data['descricao'])) ? $data['descricao'] : null;
        $this->data = (! empty($data['data'])) ? $data['data'] : null;
        $this->idTurista = (! empty($data['idTurista'])) ? $data['idTurista'] : null;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \Zend\InputFilter\InputFilterAwareInterface::setInputFilter()
     */
    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        // TODO Auto-generated method stub
        throw new \Exception("Not used");
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \Zend\InputFilter\InputFilterAwareInterface::getInputFilter()
     */
    public function getInputFilter()
    {
        // TODO Auto-generated method stub
        if (! $this->inputFilter) {
            $inputFilter = new InputFilter();
            $inputFilter->add(array(
                'name' => 'idPercurso',
                'required' => true,
                'filters' => array(
                    array(
                        'name' => 'Int'
                    )
                )
            ));
            
            $inputFilter->add(array(
                'name' => 'descricao',
                'required' => true,
                'filters' => array(
                    array(
                        'name' => 'StripTags'
                    ),
                    array(
                        'name' => 'StringTrim'
                    )
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 1,
                            'max' => 100
                        )
                    )
                )
            ));
            
            $inputFilter->add(array(
                'name' => 'data',
                'required' => true,
                'filters' => array(
                    array(
                        'name' => 'StripTags'
                    ),
                    array(
                        'name' => 'StringTrim'
                    )
                ),
                'validators' => array(
                    array(
                        'name' => 'Date',
                    )
                )
            ));
            
            $inputFilter->add(array(
                'name' => 'idTurista',
                'required' => false,
                'filters' => array(
                    array(
                        'name' => 'Int'
                    )
                )
            ));
            
            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }
}

