<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/Percurso for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
namespace Percurso\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Percurso\Form\PercursoForm;
use Percurso\Model\Percurso;

class PercursoController extends AbstractActionController
{

    protected $percursoTable;

    public function getPercursoTable()
    {
        if (! $this->percursoTable) {
            $sm = $this->getServiceLocator();
            $this->percursoTable = $sm->get('Percurso\Model\PercursoTable');
        }
        return $this->percursoTable;
    }

    public function indexAction()
    {
        return new ViewModel(array(
            'percursos' => $this->getPercursoTable()->fetchAll()
        ));
    }

    public function addAction()
    {
        $form = new PercursoForm();
        $form->get('submit')->setValue('Novo');
        
        $request = $this->getRequest();
        if ($request->isPost()) {
            $percurso = new Percurso();
            $form->setInputFilter($percurso->getInputFilter());
            $form->setData($request->getPost());
            
            if ($form->isValid()) {
                $percurso->exchangeArray($form->getData());
                $this->getPercursoTable()->savePercurso($percurso);
                
                return $this->redirect()->toRoute('percurso');
            }
        }
        return array(
            'form' => $form
        );
    }

    public function editAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);
        if (! $id) {
            return $this->redirect()->toRoute('percurso', array(
                'action' => 'index'
            ));
        }
        try {
            $percurso = $this->getPercursoTable()->getPercurso($id);
        } catch (\Exception $ex) {
            return $this->redirect()->toRoute('percurso', array(
                'action' => 'index'
            ));
        }
        $form = new PercursoForm();
        $form->bind($percurso);
        $form->get('submit')->setAttribute('value', 'Editar');
        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setInputFilter($percurso->getInputFilter());
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $this->getPercursoTable()->savePercurso($percurso);
                return $this->redirect()->toRoute('percurso');
            }
        }
        return array(
            'id' => $id,
            'form' => $form
        );
    }

    public function deleteAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);
        if (! $id) {
            return $this->redirect()->toRoute('percurso');
        }
        $request = $this->getRequest();
        if ($request->isPost()) {
            $del = $request->getPost('del', 'N�o');
            if ($del == 'Sim') {
                $id = (int) $request->getPost('id');
                $this->getPercursoTable()->deletePercurso($id);
            }
            return $this->redirect()->toRoute('percurso');
        }
        return array(
            'id' => $id,
            'percurso' => $this->getPercursoTable()->getPercurso($id)
        );
    }

    public function fooAction()
    {
        // This shows the :controller and :action parameters in default route
        // are working when you browse to /percurso/percurso/foo
        return array();
    }
}
