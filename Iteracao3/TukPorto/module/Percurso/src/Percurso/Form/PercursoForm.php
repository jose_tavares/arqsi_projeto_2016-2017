<?php
namespace Percurso\Form;

use Zend\Form\Form;

class PercursoForm extends Form
{

    public function __construct($name = null)
    {
        parent::__construct('percurso');
        
        $this->add(array(
            'name' => 'idPercurso',
            'type' => 'Hidden'
        ));
        
        $this->add(array(
            'name' => 'descricao',
            'type' => 'Text',
            'options' => array(
                'label' => 'Descricao'
            )
        ));
        
        $this->add(array(
            'name' => 'data',
            'type' => 'Date',
            'options' => array(
                'label' => 'Data'
            ),
            'attributes' => array(
                'min' => '2012-01-01',
                'max' => '2020-01-01',
                'step' => '1'
            )
        ));
        
        $this->add(array(
            'name' => 'idTurista',
            'type' => 'Text',
            'options' => array(
                'label' => 'Turista'
            )
        ));
        
        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Go',
                'id' => 'submitbutton'
            )
        ));
    }
}