﻿using DataAccessLibrary.Models;
using System.Data.Entity;

namespace DataAccessLibrary.DAL
{
    public class DataAccessLibraryDbContext : DbContext
    {
        public DataAccessLibraryDbContext() : base("Datum") { }
        public DbSet<Poi> Pois { get; set; }
        public DbSet<Local> Locais { get; set; }
        public DbSet<Meteorologia> Meteorologias { get; set; }
    }
}
