namespace DataAccessLibrary.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class LocalIdOptional : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Meteorologias", "LocalId", "dbo.Locals");
            DropForeignKey("dbo.Pois", "LocalId", "dbo.Locals");
            DropIndex("dbo.Meteorologias", new[] { "LocalId" });
            DropIndex("dbo.Pois", new[] { "LocalId" });
            AlterColumn("dbo.Meteorologias", "LocalId", c => c.Int());
            AlterColumn("dbo.Pois", "LocalId", c => c.Int());
            CreateIndex("dbo.Meteorologias", "LocalId");
            CreateIndex("dbo.Pois", "LocalId");
            AddForeignKey("dbo.Meteorologias", "LocalId", "dbo.Locals", "LocalId");
            AddForeignKey("dbo.Pois", "LocalId", "dbo.Locals", "LocalId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Pois", "LocalId", "dbo.Locals");
            DropForeignKey("dbo.Meteorologias", "LocalId", "dbo.Locals");
            DropIndex("dbo.Pois", new[] { "LocalId" });
            DropIndex("dbo.Meteorologias", new[] { "LocalId" });
            AlterColumn("dbo.Pois", "LocalId", c => c.Int(nullable: false));
            AlterColumn("dbo.Meteorologias", "LocalId", c => c.Int(nullable: false));
            CreateIndex("dbo.Pois", "LocalId");
            CreateIndex("dbo.Meteorologias", "LocalId");
            AddForeignKey("dbo.Pois", "LocalId", "dbo.Locals", "LocalId", cascadeDelete: true);
            AddForeignKey("dbo.Meteorologias", "LocalId", "dbo.Locals", "LocalId", cascadeDelete: true);
        }
    }
}
