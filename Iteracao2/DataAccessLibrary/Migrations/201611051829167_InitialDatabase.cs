namespace DataAccessLibrary.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialDatabase : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Locals",
                c => new
                    {
                        LocalId = c.Int(nullable: false, identity: true),
                        GPS_Lat = c.Double(nullable: false),
                        GPS_Long = c.Double(nullable: false),
                        Nome = c.String(nullable: false, maxLength: 60),
                    })
                .PrimaryKey(t => t.LocalId);
            
            CreateTable(
                "dbo.Meteorologias",
                c => new
                    {
                        MeteorologiaId = c.Int(nullable: false, identity: true),
                        Data_de_leitura = c.DateTime(nullable: false),
                        Hora_de_leitura = c.DateTime(nullable: false),
                        Temp = c.Single(nullable: false),
                        Humidade = c.Int(nullable: false),
                        Vento = c.Int(nullable: false),
                        Pressao = c.Int(nullable: false),
                        No = c.Int(nullable: false),
                        No2 = c.Int(nullable: false),
                        Co2 = c.Int(nullable: false),
                        LocalId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.MeteorologiaId)
                .ForeignKey("dbo.Locals", t => t.LocalId, cascadeDelete: true)
                .Index(t => t.LocalId);
            
            CreateTable(
                "dbo.Pois",
                c => new
                    {
                        PoiId = c.Int(nullable: false, identity: true),
                        Nome = c.String(nullable: false, maxLength: 60),
                        Descricao = c.String(),
                        LocalId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.PoiId)
                .ForeignKey("dbo.Locals", t => t.LocalId, cascadeDelete: true)
                .Index(t => t.LocalId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Pois", "LocalId", "dbo.Locals");
            DropForeignKey("dbo.Meteorologias", "LocalId", "dbo.Locals");
            DropIndex("dbo.Pois", new[] { "LocalId" });
            DropIndex("dbo.Meteorologias", new[] { "LocalId" });
            DropTable("dbo.Pois");
            DropTable("dbo.Meteorologias");
            DropTable("dbo.Locals");
        }
    }
}
