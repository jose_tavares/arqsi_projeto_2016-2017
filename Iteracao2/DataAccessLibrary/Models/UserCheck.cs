﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLibrary.Models
{
    class UserCheck
    {
        /*public static Boolean isCreator(String uName, Poi p)
        {
            if (p.CreatorName == uName)
                return true;
            return false;
        }*/

        public static Boolean isEditor(System.Security.Principal.IPrincipal u)
        {
            if (u.IsInRole("Editor") || u.IsInRole("Creator"))
            {
                return true;
            }
            return false;
        }

        public static Boolean hasValidToken(String s)
        {
            if (s == "TukPorto" || s == "Widget")
            {
                return true;
            }
            return false;
        }
    }
}
