﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DataAccessLibrary.Models
{
    public class Meteorologia
    {
        public int MeteorologiaId { get; set; }

        [Display(Name = "Data de leitura"), DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime Data_de_leitura { get; set; }

        [Display(Name = "Hora de leitura"), DataType(DataType.Time)]
        public DateTime Hora_de_leitura { get; set; }

        [Display(Name = "Temperatura")]
        public float Temp { get; set; }

        [Display(Name = "Humidade")]
        public int Humidade { get; set; }

        [Display(Name = "Vento")]
        public int Vento { get; set; }

        [Display(Name = "Pressão")]
        public int Pressao { get; set; }

        [Display(Name = "NO")]
        public int No { get; set; }

        [Display(Name = "NO2")]
        public int No2 { get; set; }

        [Display(Name = "CO2")]
        public int Co2 { get; set; }

        public int? LocalId { get; set; }
        public Local Local { get; set; }
    }
}
