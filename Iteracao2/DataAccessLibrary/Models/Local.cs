﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DataAccessLibrary.Models
{
    public class Local
    {
        public int LocalId { get; set; }

        [Display(Name = "GPS Latitude")]
        public double GPS_Lat { get; set; }

        [Display(Name = "GPS Longitude")]
        public double GPS_Long { get; set; }

        [Required, Display(Name = "Local"), StringLength(60, MinimumLength = 3)]
        public string Nome { get; set; }

        public ICollection<Poi> Pois { get; set; }
        public ICollection<Meteorologia> Meteorologias { get; set; }
    }
}
