﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLibrary.Models
{
    class User
    {
        public String ID { get; set; }
        public String Nome { get; set; }
        public String Email { get; set; }
        public String Password { get; set; }
    }
}
