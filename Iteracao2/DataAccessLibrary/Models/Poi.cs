﻿using System.ComponentModel.DataAnnotations;

namespace DataAccessLibrary.Models
{
    public class Poi
    {
        public int PoiId { get; set; }

        [Required, Display(Name = "POI"), StringLength(60, MinimumLength = 3)]
        public string Nome { get; set; }

        [Display(Name = "Descrição")]
        public string Descricao { get; set; }

        public int? LocalId { get; set; }
        public virtual Local Local { get; set; }
    }
}
