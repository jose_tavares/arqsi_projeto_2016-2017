﻿using System.Data.Entity;
using System.Threading.Tasks;
using System.Net;
using System.Web.Mvc;
using DataAccessLibrary.DAL;
using DataAccessLibrary.Models;

namespace Lugares.Controllers
{
    public class MeteorologiaController : Controller
    {
        private DataAccessLibraryDbContext db = new DataAccessLibraryDbContext();

        // GET: Meteorologia
        public async Task<ActionResult> Index()
        {
            var meteorologias = db.Meteorologias.Include(m => m.Local);
            return View(await meteorologias.ToListAsync());
        }

        // GET: Meteorologia/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Meteorologia meteorologia = await db.Meteorologias.FindAsync(id);
            if (meteorologia == null)
            {
                return HttpNotFound();
            }
            return View(meteorologia);
        }

        // GET: Meteorologia/Create
        public ActionResult Create()
        {
            ViewBag.LocalId = new SelectList(db.Locais, "LocalId", "Nome");
            return View();
        }

        // POST: Meteorologia/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "MeteorologiaId,Data_de_leitura,Hora_de_leitura,Temp,Humidade,Vento,Pressao,No,No2,Co2,LocalId")] Meteorologia meteorologia)
        {
            if (ModelState.IsValid)
            {
                db.Meteorologias.Add(meteorologia);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.LocalId = new SelectList(db.Locais, "LocalId", "Nome", meteorologia.LocalId);
            return View(meteorologia);
        }

        // GET: Meteorologia/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Meteorologia meteorologia = await db.Meteorologias.FindAsync(id);
            if (meteorologia == null)
            {
                return HttpNotFound();
            }
            ViewBag.LocalId = new SelectList(db.Locais, "LocalId", "Nome", meteorologia.LocalId);
            return View(meteorologia);
        }

        // POST: Meteorologia/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "MeteorologiaId,Data_de_leitura,Hora_de_leitura,Temp,Humidade,Vento,Pressao,No,No2,Co2,LocalId")] Meteorologia meteorologia)
        {
            if (ModelState.IsValid)
            {
                db.Entry(meteorologia).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.LocalId = new SelectList(db.Locais, "LocalId", "Nome", meteorologia.LocalId);
            return View(meteorologia);
        }

        // GET: Meteorologia/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Meteorologia meteorologia = await db.Meteorologias.FindAsync(id);
            if (meteorologia == null)
            {
                return HttpNotFound();
            }
            return View(meteorologia);
        }

        // POST: Meteorologia/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Meteorologia meteorologia = await db.Meteorologias.FindAsync(id);
            db.Meteorologias.Remove(meteorologia);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
