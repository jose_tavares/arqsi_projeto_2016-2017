﻿using System.Data.Entity;
using System.Threading.Tasks;
using System.Net;
using System.Web.Mvc;
using DataAccessLibrary.DAL;
using DataAccessLibrary.Models;

namespace Lugares.Controllers
{
    public class PoiController : Controller
    {
        private DataAccessLibraryDbContext db = new DataAccessLibraryDbContext();

        // GET: Poi
        public async Task<ActionResult> Index()
        {
            var pois = db.Pois.Include(p => p.Local);
            return View(await pois.ToListAsync());
        }

        // GET: Poi/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Poi poi = await db.Pois.FindAsync(id);
            if (poi == null)
            {
                return HttpNotFound();
            }
            return View(poi);
        }

        // GET: Poi/Create
        public ActionResult Create()
        {
            ViewBag.LocalId = new SelectList(db.Locais, "LocalId", "Nome");
            return View();
        }

        // POST: Poi/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "PoiId,Nome,Descricao,LocalId")] Poi poi)
        {
            if (ModelState.IsValid)
            {
                db.Pois.Add(poi);
                await db.SaveChangesAsync();
                return RedirectToAction("Index", "Local");
            }

            ViewBag.LocalId = new SelectList(db.Locais, "LocalId", "Nome", poi.LocalId);
            return View(poi);
        }

        // GET: Poi/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Poi poi = await db.Pois.FindAsync(id);
            if (poi == null)
            {
                return HttpNotFound();
            }
            ViewBag.LocalId = new SelectList(db.Locais, "LocalId", "Nome", poi.LocalId);
            return View(poi);
        }

        // POST: Poi/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "PoiId,Nome,Descricao,LocalId")] Poi poi)
        {
            if (ModelState.IsValid)
            {
                db.Entry(poi).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.LocalId = new SelectList(db.Locais, "LocalId", "Nome", poi.LocalId);
            return View(poi);
        }

        // GET: Poi/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Poi poi = await db.Pois.FindAsync(id);
            if (poi == null)
            {
                return HttpNotFound();
            }
            return View(poi);
        }

        // POST: Poi/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Poi poi = await db.Pois.FindAsync(id);
            db.Pois.Remove(poi);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
