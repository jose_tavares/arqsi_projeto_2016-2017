﻿using DataAccessLibrary.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Mvc;
using Visita.Helpers;

namespace Visita.Controllers
{
    public class VisitaController : Controller
    {
        // GET: Visita
        public async Task<ActionResult> Index()
        {
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Meteorologia");
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var meteorologias = JsonConvert.DeserializeObject<IEnumerable<Meteorologia>>(content);
                return View(meteorologias);
                //return Redirect("http://localhost:22603/meteorologia");
            }
            else
            {
                return Content("Ocorreu um erro: " + response.StatusCode);
            }
        }

        // GET: Visita
        public async Task<ActionResult> IndexFiltro(DateTime? DataInicio, DateTime? DataFim, char?[] NomePoi)
        {
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Meteorologia");
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var meteorologias = JsonConvert.DeserializeObject<IEnumerable<Meteorologia>>(content);

                List<Meteorologia> filtro = new List<Meteorologia>();
                foreach (var item in meteorologias)
                {
                    if (DataInicio >= item.Data_de_leitura && DataFim <= item.Data_de_leitura)
                    {
                        filtro.Add(item);
                    }

                    //foreach(var itemPoi in item.Local.Pois)
                    //{
                    //    if(itemPoi.Nome.Contains(string.Join("", NomePoi)))
                    //    {
                    //        filtro.Add(item);
                    //    }
                    //}

                }
                return /*filtro.Count > 0 ?*/ View(filtro) /*: View(meteorologias)*/;
            }
            else
            {
                return Content("Ocorreu um erro: " + response.StatusCode);
            }
        }
    }
}