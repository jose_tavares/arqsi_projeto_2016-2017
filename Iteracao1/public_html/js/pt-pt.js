/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*  Urls SmartCity */
var urlSensor = "http://phpdev2.dei.isep.ipp.pt/~nsilva/smartcity/sensores.php";
var urlFacetas = "http://phpdev2.dei.isep.ipp.pt/~arqsi/smartcity/facetasByNomeSensor.php?sensor=";
var urlValoresPorFaceta = "http://phpdev2.dei.isep.ipp.pt/~arqsi/smartcity/valoresFacetadoSensor.php?sensor=";
var urlTipoFacetaByName = "http://phpdev2.dei.isep.ipp.pt/~arqsi/smartcity/tipoFacetaByName.php?nomefaceta=";
var urlMaximoFaceta = "http://phpdev2.dei.isep.ipp.pt/~arqsi/smartcity/maxFaceta.php?sensor=";
var urlMinimoFaceta = "http://phpdev2.dei.isep.ipp.pt/~arqsi/smartcity/minFaceta.php?sensor=";
var urlValoresTodasFacetas = "http://phpdev2.dei.isep.ipp.pt/~arqsi/smartcity/valoresDeSensor.php?sensor=";
var urlValoresMeteorologia = "http://localhost:23070/api/meteorologia";

/* Nomes variáveis */
var qualidadeArStr = "Qualidade_do_ar";
var fotoStr = "foto";
var gpsStr = "coordenadas";
var meteoStr = "meteorologia";
//var gpsLatitudeStr = "GPS_Latitude";
//var gpsLongitudeStr = "GPS_Longitude";
var dataLeituraStr = "Data_de_leitura";
var horaLeituraStr = "Hora_de_leitura";
var tempStr = "Temp";
var precoStr = "Preco";
var transitoStr = "Transito";
var continuoStr = "contínuo";
var tempUnit = " ºC";
var precoUnit = " €";
var minimoStr = "Min: ";
var maximoStr = "Max: ";

