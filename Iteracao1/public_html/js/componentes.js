function createTable(facetas, results) {

    var articleResult = document.getElementById("resultados-pane");

    erasePrevTable(articleResult);

    var tableResults = document.createElement("Table");
    var tableHead = document.createElement("thead");
    var tableRow = document.createElement("tr");

    tableResults.classList.add("table");
    tableResults.classList.add("table-striped");
    tableResults.classList.add("table-bordered");
    tableResults.id = "resultados";
    tableResults.style.display = "block";

    createTableHeaders(facetas, tableRow); // call createTableHeaders function      

    tableHead.appendChild(tableRow);
    tableResults.appendChild(tableHead);

    var tableBody = document.createElement("tbody");

    createTableDef(facetas, results, tableBody); // call createTableDef function     

    tableResults.appendChild(tableBody);
    articleResult.appendChild(tableResults);
}

function createTableHeaders(facetas, tableRow) {
    for (i = 0; i < facetas.length; i++) {
        var tableHeader = document.createElement("th");
        var txtHeader = document.createElement("h5");
        var nameHeader = document.createTextNode(camelCase(facetas[i], '_'));

        txtHeader.appendChild(nameHeader);
        tableHeader.appendChild(txtHeader);
        tableRow.appendChild(tableHeader);
    }
}

function createTableDef(facetas, results, tableBody) {   

    var cont = 0;
    var cont2 = results.length / facetas.length;
    for (var i = 0; i < cont2; i++) {
        createTableRow(cont,facetas, results, tableBody);
        cont += facetas.length;
    }
}
function createTableRow(cont,facetas,results, tableBody){

        var tableRow = document.createElement("tr");
        for (var j = cont; j < cont + facetas.length; j++) {
            var tableDef = document.createElement("td");
            tableDef.style = "padding: 3px;";
            var auxResult= results[j];
            makeFilter(auxResult,tableDef);

            tableRow.appendChild(tableDef);
            tableBody.appendChild(tableRow);
        }
}
//Testar nova função!
function makeFilter(auxResult, tableDef){

    
    if (!auxResult.includes("http")) {
                var txtDef = document.createElement("h6");
                var tdText = document.createTextNode(auxResult);

                txtDef.style = "margin-top: 5px; margin-bottom: 5px";
                txtDef.appendChild(tdText);
                tableDef.appendChild(txtDef);

            } else {
                var aLink = document.createElement("a");
                var imgSrc = document.createElement("img");

                aLink.href = auxResult;
                aLink.target = "_blank";
                imgSrc.src = auxResult;

                aLink.appendChild(imgSrc);
                tableDef.appendChild(aLink);
            }
   
}

function erasePrevTable(articleResult) {
    var existeTabelaAnterior = document.getElementById("resultados");
    if (existeTabelaAnterior !== null) {
        articleResult.removeChild(existeTabelaAnterior);
    }
}

// A checkbox já esta a funcionar.
function createCheckBox(getById, sensor, faceta, value, titulo) {

    // criar elementos  
    var newDiv = document.createElement("div");
    var newLabel = document.createElement("label");
    var newInput = document.createElement("input");
    var text = document.createTextNode(titulo);


    newInput.id = sensor + faceta + value;
    newInput.type = "checkbox";
    newInput.value = value;

    newDiv.appendChild(newLabel);
    newLabel.appendChild(newInput);
    newLabel.appendChild(text);
    newDiv.appendChild(newLabel);
    newDiv.classList.add("checkbox"); //class
    getById.appendChild(newDiv);

    return newInput;
}
//Combobox Pronta
function createComboBox(getById, titulo) {
    // criar elementos  
    var getById = document.getElementById(getById);
    var newDiv = document.createElement("div");
    var newButton = document.createElement("button");
    var span = document.createElement("span");
    var ul = document.createElement("ul");
    var text = document.createTextNode(titulo);

    //newButton.btn-group("btn btn-default dropdown-toggle");
    newButton.classList.add("dropdown-toggle");
    newButton.type = "button";
    newButton.setAttribute("data-toggle", "dropdown");
    span.classList.add("caret");
    ul.classList.add("dropdown-menu");

    newDiv.appendChild(newButton);
    newButton.appendChild(text);
    newButton.appendChild(span);
    newDiv.appendChild(ul);
    vetor_opcoes = ["TESTE1", "TESTE2", "TESTE3"];

    for (var x in vetor_opcoes) {
        var li = document.createElement("li");
        var a = document.createElement("a");
        ul.appendChild(li);
        a.setAttribute("href", "#");
        $(a).append(vetor_opcoes[x]);
        li.appendChild(a);
        //a.onclick = function(){ selectElement(a); };
    }

    newDiv.classList.add("dropdown");
    getById.appendChild(newDiv);
}


//Slider pronto
function createSlider(getById, min, max) {
    var newInput = document.createElement("input");

    newInput.type = "range";
    newInput.min = min;
    newInput.max = max;
    newInput.step = "0.01";
    newInput.value = max;

    getById.appendChild(newInput);
    return newInput;
}

//Slider pronto
function createSliderMin(getById, min, max) {
    var newInput = document.createElement("input");

    newInput.type = "range";
    newInput.min = min;
    newInput.max = max;
    newInput.step = "0.01";
    newInput.value = min;

    getById.appendChild(newInput);
    return newInput;
}
function createSliderMax(getById, min, max) {
    var newInput = document.createElement("input");

    newInput.type = "range";
    newInput.min = min;
    newInput.max = max;
    newInput.step = "0.01";
    newInput.value = max;

    getById.appendChild(newInput);
    return newInput;
}

//Data pronto
function createDate(getById) {

    var newInput = document.createElement("input");

    newInput.type = "date";
    newInput.classList.add("form-control");
    getById.appendChild(newInput);
    return newInput;
}

//Hora pronto
function createTime(getById) {

    var newInput = document.createElement("input");

    newInput.type = "time";
    newInput.step = "1";
    newInput.classList.add("form-control");
    getById.appendChild(newInput);
    return newInput;
}
    