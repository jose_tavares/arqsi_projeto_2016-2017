/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*  variaveis globais   */
var sensores = [];
var facetas = {};
var facetasIndex = {};
var grandezaFacetas = {};
var tipoFacetas = {};
var valoresFaceta = {};
var semanticaFacetas = {};
var co2 = {};
var data = {};
var hora = {};
var humidade = {};
//var local = {};
var localId = {};
var meteorologiaId ={};
var no = {};
var no2 = {};
var pressao = {};
var temperatura = {};
var vento = {};
var resultados = {};


/*  Métodos para pedidos AJAX   */
function createXmlHttpRequestObject() {
    var objXMLHttpRequest = null;
    if (window.XMLHttpRequest) {
        objXMLHttpRequest = new XMLHttpRequest();
    } else if (window.ActiveXObjet) {
        objXMLHttpRequest = new ActiveXObjet("Microsoft.XMLHTTP");
    }
    if (objXMLHttpRequest === null) {
        alert("Erro ao criar o objecto XMLHttpRequest!");
    }
    return objXMLHttpRequest;
}

function makeXmlHttpGetCall(url, params, async, xml, callback, args) {
    var xmlHttpObj = createXmlHttpRequestObject();
    if (xmlHttpObj) {
        xmlHttpObj.open("Get", url, async);
        xmlHttpObj.onreadystatechange = function () {
            if (xmlHttpObj.readyState === 4 && xmlHttpObj.status === 200) {
                var response;
                if (xml === true)
                    response = xmlHttpObj.responseXML;
                else
                    response = xmlHttpObj.responseText;
                callback(response, args);
            }
        };
        xmlHttpObj.send(params);
    }
}

function atualizarSensores(response) {
    var nosSensores = response.getElementsByTagName("nome");
    var i = 0;
    for (i = 0; i < nosSensores.length; i++) {
        sensores[i] = nosSensores[i].childNodes[0].nodeValue;
        makeXmlHttpGetCall(urlFacetas + sensores[i], null, true, true, atualizarFacetas, sensores[i]);
        preencherSensores(sensores[i]);
    }
    sensores[nosSensores.length] = meteoStr;
    makeXmlHttpGetCall(urlValoresMeteorologia, null, true, true, atualizarFacetasMeteorologia, sensores[nosSensores.length]);
    preencherSensores(sensores[nosSensores.length]);
}

function atualizarFacetas(response, sensor) {
    var noFacetas = response.getElementsByTagName("CampoBD");
    var nosGrandezas = response.getElementsByTagName("Grandeza");
    var nosTipos = response.getElementsByTagName("Tipo");
    var nosSemantica = response.getElementsByTagName("Semantica");

    facetas[sensor] = [];
    facetasIndex[sensor] = [];
    grandezaFacetas[sensor] = [];
    tipoFacetas[sensor] = [];
    semanticaFacetas[sensor] = [];

    for (var i = 0; i < noFacetas.length; i++) {
        facetas[sensor][i] = noFacetas[i].childNodes[0].nodeValue;
        facetasIndex[sensor][i] = i;
        grandezaFacetas[sensor][i] = nosGrandezas[i].childNodes[0].nodeValue;
        tipoFacetas[sensor][i] = nosTipos[i].childNodes[0].nodeValue;
        semanticaFacetas[sensor][i] = nosSemantica[i].childNodes[0].nodeValue;

        var args2 = [sensor, facetas[sensor][i], grandezaFacetas[sensor][i]];
        makeXmlHttpGetCall(urlValoresPorFaceta + sensor + "&faceta=" + facetas[sensor][i],
                null, true, false, atualizarValoresFaceta, args2);
    }

    preencherFacetas(sensor, facetas[sensor], semanticaFacetas[sensor]);
}

function atualizarFacetasMeteorologia(response, sensor) {
    var nosMeteo = response.getElementsByTagName("Meteorologia");
    var nosCO2 = response.getElementsByTagName("Co2");
    var nosData = response.getElementsByTagName("Data_de_leitura");
    var nosHora = response.getElementsByTagName("Hora_de_leitura");
    var nosHumidade = response.getElementsByTagName("Humidade");
    var nosLocalId = response.getElementsByTagName("LocalId");
    var nosMeteoId = response.getElementsByTagName("MeteorologiaId");
    var nosNo = response.getElementsByTagName("No");
    var nosNo2 = response.getElementsByTagName("No2");
    var nosPressao = response.getElementsByTagName("Pressao");
    var nosTemperatura = response.getElementsByTagName("Temp");
    var nosVento = response.getElementsByTagName("Vento");
   
    var facetasMeteo = ["CO2","Data","Hora","Humidade","Id: local", "Id: meteorologia", "NO", "NO2", "Pressao",
    "Temperatura", "Vento"];
    
    data[sensor] = []; hora[sensor] = []; co2[sensor] = []; humidade[sensor] = []; localId[sensor] = []; meteorologiaId[sensor] = [];
    no[sensor] = []; no2[sensor] = []; pressao[sensor] = []; temperatura[sensor] = []; vento[sensor] = [];
 

    var resultados =[];
    for(var i = 0; i<nosMeteo.length; i++){
        co2[sensor][i] = nosCO2[i].childNodes[0].nodeValue;
        data[sensor][i] = nosData[i].childNodes[0].nodeValue;
        //split da data
        data[sensor][i] = data[sensor][i].split("T",1);
        hora[sensor][i] = nosHora[i].childNodes[0].nodeValue;
        //split hora
        var temp_hora = hora[sensor][i].split("T");
        hora[sensor][i] = temp_hora[1];
        
        humidade[sensor][i] = nosHumidade[i].childNodes[0].nodeValue;
        localId[sensor][i] = nosLocalId[i].childNodes[0].nodeValue;
        meteorologiaId[sensor][i] = nosMeteoId[i].childNodes[0].nodeValue;
        no[sensor][i] = nosNo[i].childNodes[0].nodeValue;
        no2[sensor][i] = nosNo2[i].childNodes[0].nodeValue;
        pressao[sensor][i] = nosPressao[i].childNodes[0].nodeValue;
        temperatura[sensor][i] = nosTemperatura[i].childNodes[0].nodeValue;
        vento[sensor][i] = nosVento[i].childNodes[0].nodeValue;
              
        resultados = resultados.concat([co2[sensor][i],data[sensor][i],hora[sensor][i],humidade[sensor][i],
        localId[sensor][i],meteorologiaId[sensor][i],no[sensor][i],no2[sensor][i],pressao[sensor][i],
        temperatura[sensor][i],vento[sensor][i]]);
    
    }   
     preencherFacetas(sensor, facetasMeteo, meteoStr, resultados);
     validarGrandezasMeteorologia(sensor, facetasMeteo, resultados);
}

function validarGrandezasMeteorologia(sensor, facetasMeteo, resultados){
    
    var temp_temp;
    var temp_co2;
    var temp_data;
    var temp_hora;
    

    for(var i = 0; i<facetasMeteo.length; i++){
        
        if(facetasMeteo[i] === "Temperatura"){
            temp_temp=i;

        }else if(facetasMeteo[i] === "CO2"){
            temp_co2=i;
        }else if(facetasMeteo[i] === "Data"){
            temp_data=i;
        }else if(facetasMeteo[i] === "Hora"){
            temp_hora=i;
        }
    }
        constroiVetoresFiltragem(sensor, facetasMeteo, resultados, temp_temp, temp_co2, temp_data, temp_hora);
        
    }
    
function constroiVetoresFiltragem(sensor, facetasMeteo, resultados, temp_temp, temp_co2, temp_data, temp_hora){
        
    var grandezaFacetasMeteo= "contínuo";  
    var valoresCo2 = [];
    var valoresTemp = [];
    var valoresData = [];
    var valoresHora = [];
    
        for(var j= 0; j < (resultados.length/facetasMeteo.length); j++){
            
            valoresTemp[j] = resultados[temp_temp+(j*facetasMeteo.length)];
            valoresCo2[j] = resultados[temp_co2+(j*facetasMeteo.length)];
            valoresData[j] = resultados[temp_data+(j*facetasMeteo.length)];
            valoresHora[j] = resultados[temp_hora+(j*facetasMeteo.length)];
        }
        for(var i = 0; i<facetasMeteo.length; i++){
        
            if(facetasMeteo[i] === "Temperatura"){          
                preencherValoresFaceta(sensor,facetasMeteo[i],valoresTemp,grandezaFacetasMeteo,resultados, facetasMeteo);
            }else if(facetasMeteo[i] === "CO2"){
                preencherValoresFaceta(sensor,facetasMeteo[i],valoresCo2,grandezaFacetasMeteo,resultados, facetasMeteo);
            }else if(facetasMeteo[i] === "Data"){
                preencherValoresFaceta(sensor,facetasMeteo[i],valoresData,grandezaFacetasMeteo,resultados, facetasMeteo);
            }else if(facetasMeteo[i] === "Hora"){
                preencherValoresFaceta(sensor,facetasMeteo[i],valoresHora,grandezaFacetasMeteo,resultados, facetasMeteo);
            }
        }
       
    }

function atualizarValoresFaceta(response, args) { // args = [sensor, facetas[sensor][i], grandezaFacetas[sensor][i]]]
    var faceta = args[1];
    valoresFaceta[faceta] = [];
    var linha = JSON.parse(response);
    for (var i = 0; i < linha.length; i++) {
        valoresFaceta[faceta][i] = linha[i];
    }
    valoresFaceta[faceta].sort();
    preencherValoresFaceta(args[0], args[1], valoresFaceta[faceta], args[2]);
}

function preencherSensores(sensor) {
    var ulSensor = document.getElementById("ulSensor");
    var liSensor = document.createElement("li");
    var aSensor = document.createElement("a");
    var txtSensor = document.createElement("h6");
    var nomeSensor = document.createTextNode(camelCase(sensor, '_'));

    liSensor.id = "li" + sensor;
    liSensor.classList.add("menu");
    aSensor.href = "#";
    aSensor.id = "a" + sensor;
    aSensor.setAttribute("data-toggle", "tab");

    aSensor.appendChild(txtSensor);
    txtSensor.appendChild(nomeSensor);
    liSensor.appendChild(aSensor);
    ulSensor.appendChild(liSensor);
}

function preencherFacetas(sensor, facetas, semantica, resultados) {
    
    var articleFaceta = document.getElementById("articleFaceta");
    var aSensor = document.getElementById("a" + sensor);
    var spanSensor = document.createElement("span");
    spanSensor.id = "span" + sensor;
    spanSensor.style.display = "none";

    for (var i = 0; i < facetas.length; i++) {
        var spanSensorFaceta = document.createElement("span");
        spanSensorFaceta.id = "span" + sensor + facetas[i];
        inserirFaceta(facetas[i], sensor, spanSensor, spanSensorFaceta, semantica[i]);
    }
    articleFaceta.appendChild(spanSensor);
    aSensorOnClick(articleFaceta, spanSensor, aSensor, sensor, facetas, resultados);
}

function inserirFaceta(faceta, sensor, spanSensor, spanSensorFaceta, semantica) {
    if (semantica !== fotoStr && semantica !== gpsStr && faceta!=="Humidade" && faceta !== "Id: local"
            && faceta !== "Id: meteorologia" && faceta !== "NO" && faceta !== "NO2" && faceta !== "Pressao" 
            && faceta !== "Vento" && faceta !== "Hora") {
        var aFaceta = document.createElement("a");
        aFaceta.id = "a" + sensor + faceta;
        aFaceta.href = "#";
        var nomeFaceta = document.createTextNode(camelCase(faceta, '_'));
        aFaceta.appendChild(nomeFaceta);
        spanSensorFaceta.appendChild(aFaceta);
        spanSensor.appendChild(spanSensorFaceta);
        spanSensor.appendChild(document.createElement("br"));
    }
}

function aSensorOnClick(articleFaceta, spanSensor, aSensor, sensor, facetas, resultados) {
    aSensor.onclick = function () {
        var childrenArticleFaceta = articleFaceta.children;
        for (var j = 0; j < childrenArticleFaceta.length; j++) {
            childrenArticleFaceta[j].style.display = "none";
        }
        if (spanSensor !== null) {
            eventoSpanSensor(spanSensor);
        }

        createTableTrigger(sensor, facetas, resultados);
    };
}

function eventoSpanSensor(spanSensor) {
    if (spanSensor.style.display === "none") {
        spanSensor.style.display = "block";
    } else {
        spanSensor.style.display = "none";
    }
}

function preencherValoresFaceta(sensor, faceta, valores, grandeza, resultados, facetasMeteo) {
    var spanSensorFaceta = document.getElementById("span" + sensor + faceta);
    var aFaceta = document.getElementById("a" + sensor + faceta);
    var spanFaceta = document.createElement("span");
    var paragrafValor = document.createElement("p");

    spanFaceta.id = "span" + faceta;
    spanFaceta.style.display = "none";

    inserirValoresFaceta(sensor, faceta, grandeza, valores, paragrafValor, spanFaceta, resultados, facetasMeteo);
    spanSensorFaceta.appendChild(spanFaceta);
    aFacetaOnClick(aFaceta, spanFaceta);
}

function inserirValoresFaceta(sensor, faceta, grandeza, valores, paragrafValor, spanFaceta, resultados, facetasMeteo) {
    if (grandeza !== continuoStr /*|| faceta === gpsStr*/) {
        inserirFacetaDiscreta(sensor, faceta, valores, paragrafValor, spanFaceta);
    } else if (faceta === dataLeituraStr || faceta === "Data") {
        paragrafValor.id = "paragraf" + sensor + faceta;
        var data = createDate(paragrafValor);
        spanFaceta.appendChild(paragrafValor);
        filtrarValoresData(data, sensor, faceta, resultados, facetasMeteo, valores);
    } else if (faceta === horaLeituraStr || faceta === "Hora") {
        paragrafValor.id = "paragraf" + sensor + faceta;
        var tempo = createTime(paragrafValor);
        spanFaceta.appendChild(paragrafValor);
        filtrarValoresTempo(tempo, sensor, faceta, resultados, facetasMeteo, valores);
    } else {
        paragrafValor.id = "paragraf" + sensor + faceta;
        //  alternativa: Math.min.apply(null, valores), Math.max.apply(null, valores)
        var min, max;
        definirMinimo(sensor, faceta, min);
        definirMaximo(sensor, faceta, max);

        definirUnidade(paragrafValor, sensor, faceta, valores, resultados, facetasMeteo);
        spanFaceta.appendChild(paragrafValor);
    }
}

function inserirFacetaDiscreta(sensor, faceta, valores, paragrafValor, spanFaceta) {
    
    for (var i = 0; i < valores.length; i++) {
        paragrafValor.id = "paragraf" + sensor + faceta + valores[i];
        var checkBox = createCheckBox(paragrafValor, sensor, faceta, valores[i], camelCase(valores[i], '_'));
        spanFaceta.appendChild(paragrafValor);
        filtrarValoresCheckBox(checkBox, sensor, faceta);
    }
}

function definirUnidade(paragrafValor, sensor, faceta, valores, resultados, facetasMeteo) {
    if (faceta === tempStr || faceta === "Temperatura") { 
        inserirUnidade(tempUnit, sensor, faceta, valores, paragrafValor, resultados, facetasMeteo);
    } else if (faceta === precoStr) {
        inserirUnidade(precoUnit, sensor, faceta, valores, paragrafValor);
    } else {
        inserirUnidade("", sensor, faceta, valores, paragrafValor, resultados, facetasMeteo);
    }
}

function inserirUnidade(unid, sensor, faceta, valores, paragrafValor, resultados, facetasMeteo) {
    var span = document.createElement("span");
    var paragrafMinimo = document.createElement("p");
    var paragrafMaximo = document.createElement("p");
    var paragrafSelect_min = document.createElement("p");
    var paragrafSelect_max = document.createElement("p");

    span.id = "spanSlider" + sensor + faceta;
    span.style.display = "block";
    span.style.overflow = "hidden";
    span.style.padding = "0 4px 0 6px";

    paragrafMinimo.appendChild(document.createTextNode(minimoStr + Math.min.apply(null, valores) + unid));
    paragrafValor.appendChild(paragrafMinimo);

    var slider = createSliderMin(span, Math.min.apply(null, valores), Math.max.apply(null, valores));
    slider.style.width = "80%";

    paragrafSelect_min.appendChild(document.createTextNode(slider.value + unid));
    paragrafValor.appendChild(paragrafSelect_min);
    paragrafValor.appendChild(span);


    //inicio teste                        //Valor minimo recebido do Slider acima
    var slider_max = createSliderMax(span, slider.value, Math.max.apply(null, valores));
    slider_max.style.width = "80%";

    //Mostra temperatura escolhida pelo slider maximo
    paragrafSelect_max.appendChild(document.createTextNode(slider_max.value + unid));
    paragrafValor.appendChild(paragrafSelect_max);

    //paragrafValor.appendChild(span_max);
    //Final teste

    //Mostra o maximo de temperaturas registadas.
    paragrafMaximo.appendChild(document.createTextNode(maximoStr + Math.max.apply(null, valores) + unid));
    paragrafValor.appendChild(paragrafMaximo);

    filtrarValoresSlider(unid, slider, slider_max, paragrafSelect_min, paragrafSelect_max, sensor, faceta, resultados, valores, facetasMeteo);
}

function filtrarValoresCheckBox(checkBox, sensor, faceta) {
    //var boolean = false;
    checkBox.onclick = function () {
        var checkBoxes = document.getElementsByTagName("input");
        var queryString = '&' + faceta + "=[";
        for (var i = 0; i < checkBoxes.length; i++) {
            if (checkBoxes[i].checked) {
                queryString += checkBoxes[i].value;
                queryString += ',';
            }
        }
        queryString += "]&";
        atualizarQueryString(queryString, sensor, faceta);
    };
    //return boolean;
}
//MEXER AQUI!!
function filtrarValoresSlider(unid, slider, slider_max, paragrafSelect_min, paragrafSelect_max, sensor, faceta, resultados, valores, facetasMeteo) {
    var linha;
    var boolean = false;
    slider.onchange = function () {
        slider_max.min = slider.value;

        var anteriorSelect_min = paragrafSelect_min.childNodes[0];
        var novoSelect_min = document.createTextNode(slider.value + unid);
        paragrafSelect_min.replaceChild(novoSelect_min, anteriorSelect_min);

        var anteriorSelect_max = paragrafSelect_max.childNodes[0];
        var novoSelect_max = document.createTextNode(slider_max.value + unid);
        paragrafSelect_max.replaceChild(novoSelect_max, anteriorSelect_max);

        atualizarSliderMax(unid, slider, slider_max, paragrafSelect_max, sensor, faceta, resultados, valores, facetasMeteo);
       
        if(sensor === "meteorologia"){   
            callCreateTableFiltradoMeteorologia(valores, resultados, slider.value, slider_max.value, facetasMeteo);
        }else{
        makeXmlHttpGetCall(urlValoresTodasFacetas + sensor, null, true, false, function (response) {
            linha = JSON.parse(response);
            callCreateTableFiltrado(linha, faceta, slider.value, slider_max.value);
        }, null);
        }
        return linha;
    };
    return boolean;
}

function atualizarSliderMax(unid, slider, slider_max, paragrafSelect_max, sensor, faceta, resultados, valores, facetasMeteo) {

    slider_max.onchange = function () {

        var anteriorSelect_max = paragrafSelect_max.childNodes[0];
        var novoSelect_max = document.createTextNode(slider_max.value + unid);
        paragrafSelect_max.replaceChild(novoSelect_max, anteriorSelect_max);
        
        if(sensor === "meteorologia"){  
            callCreateTableFiltradoMeteorologia(valores, resultados, slider.value, slider_max.value, facetasMeteo);
        }else{
        makeXmlHttpGetCall(urlValoresTodasFacetas + sensor, null, true, false, function (response) {
            var linha = JSON.parse(response);
            callCreateTableFiltrado(linha, faceta, slider.value, slider_max.value);
        }, null);
    }
    };
}

function filtrarValoresData(data, sensor, faceta, resultados, facetasMeteo, valores) {
    data.onchange = function () {
        if(sensor !== meteoStr){
        var queryString = '&' + faceta + "=[";
        queryString += data.value;
        queryString += ',';
        queryString += "]&";
        atualizarQueryString(queryString, sensor, faceta);
    }else
        criarTabelaFiltradaDataHora(data, resultados, facetasMeteo, valores);
    };
}

function filtrarValoresTempo(tempo, sensor, faceta, resultados, facetasMeteo, valores) {
    tempo.onchange = function () {
        if(sensor !== meteoStr){
        var queryString = '&' + faceta + "=[";
        if (tempo.value === "[0-9]+:[0-9]+") {
            tempo.value += ":00";
        }
        queryString += tempo.value;
        queryString += "]&";
        atualizarQueryString(queryString, sensor, faceta);
    }else
        alert(tempo.value.toString());
        criarTabelaFiltradaDataHora(tempo, resultados, facetasMeteo, valores);
    };
}

function atualizarQueryString(queryString, sensor, faceta) {
    //var linha; 
    if (queryString !== '&' + faceta + "=[]&" && queryString !== '&' + faceta + "=[,]&") {
        makeXmlHttpGetCall(urlValoresTodasFacetas + sensor + queryString,
                null, true, false, function (response) {
                    var linha = JSON.parse(response);
                    callCreateTable(linha);
                }, null);
    } else {
        createTableTrigger(sensor);
    }
    //return linha;
}

//----------------------------------TESTE---------------------------------------------//
//function atualizarQueryString(queryString, sensor, faceta, boolean) {
//  
//  if(boolean === true){
//    if (queryString !== '&' + faceta + "=[]&" && queryString !== '&' + faceta + "=[,]&") {
//        makeXmlHttpGetCall(urlValoresTodasFacetas + sensor + queryString,
//                null, true, false, function (response) {
//                    var linha = JSON.parse(response);
//                    callCreateTable(linha);
//                }, null);
//    } else {
//        createTableTrigger(sensor);
//    }
//}//else
//      //if (queryString !== '&' + faceta + "=[]&" && queryString !== '&' + faceta + "=[,]&") {
//      //      callCreateTable(linha_slider);               
//    //} else {
//      //      createTableTrigger(sensor);
//    //}
//}

function aFacetaOnClick(aFaceta, spanFaceta) {
    aFaceta.onclick = function () {
        if (spanFaceta !== null) {
            if (spanFaceta.style.display === "none") {
                spanFaceta.style.display = "block";
            } else {
                spanFaceta.style.display = "none";
            }
        }
    };
}

function definirMinimo(sensor, facetaCont, min) {
    var url = urlMinimoFaceta + sensor + "&facetaCont=" + facetaCont;
    makeXmlHttpGetCall(url, null, true, false,
            function (response) {
                var linha = JSON.parse(response);
                min = parseFloat(linha['min']);
            }, null);
}

function definirMaximo(sensor, facetaCont, max) {
    var url = urlMaximoFaceta + sensor + "&facetaCont=" + facetaCont;
    makeXmlHttpGetCall(url, null, true, false,
            function (response) {
                var linha = JSON.parse(response);
                max = parseFloat(linha['max']);
            }, null);
}

function createTableTrigger(sensor, facetas, resultados) {
    
    if(sensor!=meteoStr){
    makeXmlHttpGetCall(urlValoresTodasFacetas + sensor, null, true, false, function (response) {
        var linha = JSON.parse(response);
        callCreateTable(linha);
    }, null);
    }else{
        createTable(facetas, resultados);
    }
}

function callCreateTable(linha) {
    var keys = [];
    var data = [];

    for (var index in linha)
        for (var key in linha[index])
            if (linha[index].hasOwnProperty(key)) {
                keys.push(key);
                keys = ArrNoDupe(keys);
                data.push(linha[index][key]);
            }
    createTable(keys, data, null, null);
}

function callCreateTableFiltradoMeteorologia(valores, resultados, value_min, value_max, facetasMeteo){
  
var aux;
var data = [];

for(var i=0; i<valores.length; i++){
    aux = parseFloat(valores[i]);
    if(aux>=value_min && aux<=value_max){
        for(var j=0; j<facetasMeteo.length; j++){
            data.push(resultados[i*facetasMeteo.length+j]);
        }
    }
}
createTable(facetasMeteo,data);
}
function criarTabelaFiltradaDataHora(data, resultados, facetasMeteo, valores){
    var aux_valores;
    var aux_data;
    var dados = [];
  
    aux_data = data.value.toString();    
    for(var i=0; i<valores.length; i++){
         aux_valores = valores[i].toString();
        if(aux_valores === aux_data){
            for(var j=0; j<facetasMeteo.length; j++){
                dados.push(resultados[i*facetasMeteo.length+j]);
        }
        }
    }
    createTable(facetasMeteo,dados);
}
//esta aquiii!!!
function callCreateTableFiltrado(linha, faceta, value_min, value_max) {
    var keys = [];
    var data = [];
    
    for (var index in linha)
        for (var key in linha[index])
            if (linha[index].hasOwnProperty(key)) {
                if (linha[index][faceta] >= value_min && linha[index][faceta] <= value_max && linha[index][faceta]) {
                    keys.push(key);
                    keys = ArrNoDupe(keys);
                    data.push(linha[index][key]);
                }
            }
    createTable(keys, data);
}

function camelCase(str, str2) {
    var arrayStrings = str.split(str2);
    for (var i = 0; i < arrayStrings.length; i++) {
        var charM = arrayStrings[i].charAt(0);

        if (arrayStrings[i] !== "de" && arrayStrings[i] !== "do"
                && arrayStrings[i] !== "da" && arrayStrings[i] !== "e") {
            charM = charM.toUpperCase();
            arrayStrings[i] = arrayStrings[i].substring(1, arrayStrings[i].length);
            arrayStrings[i] = charM + arrayStrings[i];

            if (arrayStrings[i] === transitoStr) {
                arrayStrings[i] = "Trânsito";
            } else if (arrayStrings[i] === precoStr) {
                arrayStrings[i] = "Preço";
            }
        }
    }
    return arrayStrings.join(" ");
}

function ArrNoDupe(a) {
    var temp = {};
    for (var i = 0; i < a.length; i++)
        temp[a[i]] = true;
    var r = [];
    for (var k in temp)
        r.push(k);
    return r;
}